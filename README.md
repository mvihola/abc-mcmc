Adaptive tolerance ABC-MCMC with post-correction estimators
===========================================================

This is an implementation of the adaptive tolerance approximate Bayesian computation Markov chain Monte Carlo (ABC-MCMC) algorithm
and the related post-correction estimators as described in (Vihola & Franks, 2019, [arXiv:1902.00412](https://arxiv.org/abs/1902.00412)).

> **NB**: See also the Julia package
[AdaptiveToleranceABC_MCMC.jl](https://github.com/mvihola/AdaptiveToleranceABC_MCMC.jl),
which is a newer version of this implementation, with slight improvements and modifications.

The implementation requires Julia packages `JLD2`, `FileIO` and `Plots`. If you do not have them installed, open Julia and do
```
using Pkg
Pkg.add("Plots")
Pkg.add("JLD2")
Pkg.add("FileIO")
```

In order to use the package, download and extract to some folder (e.g.
do `git clone https://bitbucket.org/mvihola/abc-mcmc`),
open Julia in that folder, and do the following:

```
include("Setup.jl")
include("test/Normal.jl")
using ABC
out = abc_mcmc([0.0], prior, s_obs, sim!, 10_000)
est = abc_postprocess(out, f)
using Plots, ABCPlots
p = plot_abc(est, 0.02)
plot(p..., layout=(2,1))
```

The code above runs the adaptive ABC-MCMC algorithm for the model with
log-prior density `prior`, a function simulating summaries for
given parameter `sim!`, and vector of observed summaries `s_obs`, and produces post-processing estimators
(with related 95% confidence intervals) `est`, which are visualised as
in Vihola & Franks, 2019,
[arXiv:1902.00412](https://arxiv.org/abs/1902.00412).

The corresponding regression-corrected mean estimates may be inspected by continuing with
```
tols = collect(0.02:0.01:out.cutoff.tol)
est_reg = abc_postprocess(out, f, tols; regress=true)
plot(plot_abc(est_reg, 0.02)..., layout=(2,1))
```

We may continue by inspecting the Lotka-Volterra model:
```
include("test/LotkaVolterra.jl")
out_lv = abc_mcmc(draw_from_prior(), prior, s_obs, sim!, 20_000; b=10_000)
est_lv = abc_postprocess(out_lv, f)
p = plot_abc(est_lv, 60.0); for i=1:3 plot!(p[i], ylabel="\\theta_$i") end
plot(p..., layout=(3,1))
```

The following commands reproduce the analysis in the paper (assuming
Julia is started in `test` folder).
Warning: all of these take a long time to run; possibly days.
If you have access to distributed computing facilities that use _slurm_,
there are batch files such as `run_Normal.sh`, that may (possibly) be run with
`sbatch run_Normal.sh` instead of the first line in each experiment, and then the corresponding `postprocess` script must be run before proceeding.

Normal comparison experiment:

```
include("test_Normal.jl")
include("test_Normal_plots.jl")
include("test_Normal_confidence.jl")
```

... with Gaussian cut-off:

```
include("test_Normal_GaussCutoff.jl")
include("test_Normal_GaussCutoff_plots.jl")
include("test_Normal_GaussCutoff_confidence.jl")
```

... with tolerance adaptation:
```
include("test_Normal_Adaptation.jl")
include("test_Normal_Adaptation_plots.jl")
include("test_Normal_GaussCutoff_Adaptation.jl")
include("test_Normal_GaussCutoff_Adaptation_plots.jl")
include("test_Normal_Adaptation_compare.jl")
```

Before running the Lotka-Volterra experiments, unpack the file `test/output/LotkaVolterra_ground.jld2.gz`, (using `gunzip`) or run `include("test_LotkaVolterra_ground.jl")`.

The Lotka-Volterra comparison:
```
include("test_LotkaVolterra.jl")
include("test_LotkaVolterra_plots.jl")
include("test_LotkaVolterra_confidence.jl")
```

... with adaptation:
```
include("test_LotkaVolterra_Adaptation.jl")
include("test_LotkaVolterra_Adaptation_plots.jl")
include("test_LotkaVolterra_Adaptation_compare.jl")
```

... with step size n^(-2/3):
```
include("test_LotkaVolterra_stepSize.jl")
include("test_LotkaVolterra_stepSize_plots.jl")
include("test_LotkaVolterra_stepSize_confidence.jl")
include("test_LotkaVolterra_Adaptation_compare_stepSize.jl")
```
