# Very simple implementation of some adaptive MCMC, including the AM adaptation of
# Haario, Saksman & Tamminen, Bernoulli, 2001.
module AdaptiveMCMC

export AdaptState, AdaptiveStepSize, AdaptiveMetropolis, RobustAdaptiveMetropolis, AdaptiveScalingMetropolis, AdaptiveScalingWithinAdaptiveMetropolis,
ConstantShape, draw!, adapt!

using Random
using LinearAlgebra
#import LinearAlgebra: norm, Cholesky, lowrankupdate!, lowrankdowndate!, diagm, cholesky

import Base.*
# Add definition to scalar multiplication of a Cholesky factor...
# That is, L * L.' -> a * L * L.'
function *(L::Cholesky, a::Float64)
    a>0 || error("Multiplier must be positive")
    Cholesky(sqrt(a)*L.factors, L.uplo, L.info)
end

# for identity matrix
eye(d) = diagm(0 => fill(1.0,d))

abstract type AdaptState end

abstract type StepSize end

mutable struct ConstantStepSize <: StepSize
    eta::Float64
    k::Int64
end
function ConstantStepSize(eta::Float64)
    ConstantStepSize(eta, 1)
end

mutable struct AdaptiveStepSize <: StepSize
    eta::Float64
    k::Int64
    previousIncrement::Float64
end
function AdaptiveStepSize(eta::Float64)
    AdaptiveStepSize(eta, 2, 1.0)
end

# The necessary ingredients to implement adaptive Metropolis stuff...
mutable struct AdaptiveMetropolis <: AdaptState
    m::Vector{Float64}
    L::Cholesky
    sc::Float64
    d::Int64
    u::Vector{Float64}
    y::Vector{Float64}
    dx::Vector{Float64}
    step::StepSize
end

mutable struct RobustAdaptiveMetropolis <: AdaptState
    L::Cholesky
    d::Int64
    α_opt::Float64
    u::Vector{Float64}
    y::Vector{Float64}
    dx::Vector{Float64}
    n_u::Vector{Float64}
    step::StepSize
end

mutable struct AdaptiveScalingMetropolis <: AdaptState
    sc::Float64
    d::Int64
    α_opt::Float64
    u::Vector{Float64}
    y::Vector{Float64}
    step::StepSize
end

mutable struct AdaptiveScalingWithinAdaptiveMetropolis <: AdaptState
    AM::AdaptiveMetropolis
    ASM::AdaptiveScalingMetropolis
    d::Int64
    α_opt::Float64
    u::Vector{Float64}
    y::Vector{Float64}
end

struct ConstantShape <: AdaptState
    d::Int64
    L::Cholesky
    u::Vector{Float64}
    y::Vector{Float64}
end


function next!(step::ConstantStepSize, dx::Union{Nothing,Float64})
    step.k += 1
    step.k^(-step.eta)
end

function next!(step::AdaptiveStepSize, dx::Float64)
    if (dx*step.previousIncrement < 0.0)
        step.k += 1
    end
    step.previousIncrement = dx
    step.k^(-step.eta)
end
function next!(step::AdaptiveStepSize, dx::Nothing)
    step.k^(-step.eta)
end

# Convenience outer constructor w/ given dimension
function AdaptiveScalingMetropolis(d::Int64, α_opt=0.234, sc=1.0)
    u = fill(0.0, d); y = fill(0.0, d); eta = 0.66
    AdaptiveScalingMetropolis(sc, d, α_opt, u, y, ConstantStepSize(eta))
end

# Convenience outer constructor w/ given dimension
function AdaptiveScalingWithinAdaptiveMetropolis(d::Int64, α_opt=0.234)
    AM = AdaptiveMetropolis(d)
    ASM = AdaptiveScalingMetropolis(1, α_opt)
    u = fill(0.0, d); y = fill(0.0, d)
    AdaptiveScalingWithinAdaptiveMetropolis(AM, ASM, d, α_opt, u, y)
end

# Convenience outer constructor w/ given dimension
function AdaptiveMetropolis(d::Int64)
    m = zeros(d); L = cholesky(eye(d)); sc = 2.38/sqrt(d)
    u = fill(0.0, d); y = fill(0.0, d); dx = fill(0.0, d)
    eta = 1.0
    AdaptiveMetropolis(m,L,sc,d,u,y,dx,ConstantStepSize(eta))
end

# Convenience outer constructor w/ given dimension
function RobustAdaptiveMetropolis(d::Int64, α_opt=0.234)
    L = cholesky(eye(d))
    u = fill(0.0, d); y = fill(0.0, d); dx = fill(0.0, d); n_u = fill(0.0, d)
    eta = 0.66
    RobustAdaptiveMetropolis(L, d, α_opt, u, y, dx, n_u, ConstantStepSize(eta))
end

function ConstantShape(M::Matrix{Float64})
    n, m = size(M)
    @assert n == m
    ConstantShape(n, cholesky(M), fill(0.0,n), fill(0.0,n))
end

function ConstantShape(d::Int64, sc::Float64)
    ConstantShape(sc*eye(d))
end

# Update function -- not yet implemented in the best possible way...
function adapt!(s::AdaptiveMetropolis, x::Vector{Float64}, α::Float64)
    gamma = next!(s.step, nothing)
    s.dx .= x - s.m
    s.m += gamma*s.dx
    s.dx *= sqrt(gamma)
    s.L *= 1.0-gamma
    lowrankupdate!(s.L, s.dx)
    nothing
end

function adapt!(s::ConstantShape, x::Vector{Float64}, α::Float64)
    nothing
end

function adapt!(s::RobustAdaptiveMetropolis, x::Vector{Float64}, α::Float64)
    γ = min(0.5, s.d*next!(s.step, nothing))
    norm_u = norm(s.u); norm_u += (norm_u == 0.0)
    s.n_u = s.u/norm_u
    mul!(s.dx, s.L.L, s.n_u)
    s.dx *= sqrt(γ*abs(α-s.α_opt))
    #s.dx = sqrt(γ*abs(α-s.α_opt)) * s.L.L * (s.u/n_u)
    if α >= s.α_opt
        lowrankupdate!(s.L, s.dx)
    else
        lowrankdowndate!(s.L, s.dx)
    end
end

function adapt!(s::AdaptiveScalingWithinAdaptiveMetropolis, x::Vector{Float64}, α::Float64)
    adapt!(s.AM, x, α, k)
    adapt!(s.ASM, x, α, k)
end

function adapt!(s::AdaptiveScalingMetropolis, x::Vector{Float64}, α::Float64)
    dα = α-s.α_opt
    γ = next!(s.step, dα)
    s.sc *= exp(γ*dα)
end

function draw!(s::ConstantShape, rng=Random.GLOBAL_RNG)
    randn!(rng, s.u)
    mul!(s.y, s.L.L, s.u)
    #s.y .= s.L * randn(rng, s.d)
end

function draw!(s::AdaptiveMetropolis, rng=Random.GLOBAL_RNG)
    randn!(rng, s.u)
    mul!(s.y, s.L.L, s.u)
    s.y *= s.sc
    #y .= s.sc * s.L.L * randn(rng, s.d)
end
function draw!(s::AdaptiveScalingWithinAdaptiveMetropolis, rng=Random.GLOBAL_RNG)
    randn!(rng, s.u)
    mul!(s.y, s.AM.L.L, s.u)
    s.y *= s.ASM.sc
    #y .= s.AM.sc * s.AM.L.L * randn(rng, s.AM.d)
end
function draw!(s::RobustAdaptiveMetropolis, rng::AbstractRNG=Random.GLOBAL_RNG)
    randn!(rng, s.u)
    #s.u = randn(rng, s.d)
    mul!(s.y, s.L.L, s.u)
    #y .= s.L.L * s.u
end
function draw!(s::AdaptiveScalingMetropolis, rng=Random.GLOBAL_RNG)
    randn!(rng, s.u)
    s.y .= s.sc*s.u
end

end # module
