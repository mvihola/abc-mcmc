module ABCPlots

export plot_abc

using Plots
using ABC

"""
    plot_abc(est, [min_tol])

Plot ABC post-correction estimators with related 95% confidence intervals.

# Arguments
- `est`: Output of `abc_postprocess`.
- `min_tol`: Minimum tolerance (default 0.0).
"""
function plot_abc(est::ABCEst, min_tol=0.0)
    lims(x) = (minimum(x), maximum(x))
    d, n = size(est.E)
    v = est.eps .>= min_tol
    eps_ = est.eps[v]
    p = Vector{Plots.Plot}(undef, d)
    for i in 1:d
        E_ = est.E[i,v]; L_ = est.ci_L[i,v];  U_ = est.ci_U[i,v]
        p[i] = Plots.plot(eps_, E_, ribbon=[E_-L_, U_-E_], color=:black,
                          fillalpha=0.3, legend=false, xlim=lims(eps_),
                          framestyle=:axes)
    end
    p
end

end
