module ABC
# Adaptive tolerance ABC-MCMC algorithm with post-correction

export ABCCutoff, ABCSimpleCutoff, ABCGenCutoff, ABCGaussCutoff, ABCEpaCutoff, ABCStats, ABCOut, ABCEst,
       abc_mcmc, abc_postprocess, ABCdefaultDist

using Statistics
using LinearAlgebra
using Random
using AdaptiveMCMC

# Abstract 'cutoff function' type
abstract type ABCCutoff end

# Simple cutoff at 1.0
mutable struct ABCSimpleCutoff <: ABCCutoff
    tol::Float64
    fun::Function
end

# General cutoff function
mutable struct ABCGenCutoff <: ABCCutoff
    tol::Float64
    fun::Function
end

# Outer constructors for the abstract type:
# if no function supplied, it's simple...
function ABCCutoff(tol::Float64, fun::Nothing)
    ABCSimpleCutoff(tol, x -> (x .<= 1.0))
end
function ABCCutoff(tol::Float64, fun::Function)
    ABCGenCutoff(tol, fun)
end
# Gaussian
function ABCGaussCutoff(tol::Float64)
    ABCGenCutoff(tol, x -> exp.(-0.5*x.^2))
end
# Epanechnikov
function ABCEpaCutoff(tol::Float64)
    ABCGenCutoff(tol, x -> x>=1.0 ? 0.0 : 1.0 - x^2)
end

# Convenience functions
@inline function (c::ABCGenCutoff)(x)
    c.fun(x/c.tol)
end
@inline function (c::ABCSimpleCutoff)(x)
    c.fun(x/c.tol)
end


# Stats of ABC-MCMC
struct ABCStats
    all_acc::Float64
    acc::Float64
    adapt::AdaptState
    adapt_burnin::AdaptState
    tol::Union{Nothing,Vector{Float64}}
end

# Output of ABC-MCMC
struct ABCOut
    Theta::Array{Float64}
    Dist::Vector{Float64}
    Summary::Union{Missing,Array{Float64}}
    s_obs::Vector{Float64}
    cutoff::ABCCutoff
    Stats::Union{Nothing,ABCStats}
end

# IS-estimates
struct ABCEst
    E::Matrix{Float64}
    S::Matrix{Float64}
    eps::Vector{Float64}
    ci_L::Matrix{Float64}
    ci_U::Matrix{Float64}
    normal_quantile::Float64
end

@inline function ABCdefaultDist(x::Vector{Float64}, y::Vector{Float64})
    s = 0.0
    for i in 1:length(x)
        s += (x[i] - y[i])^2
    end
    isnan(s) ? Inf64 : sqrt(s)
end

"""
    out = abc_mcmc(theta0, prior, s_obs, sim!, n; [tol=0.0, b=0.1n, adapt_cov=true,
             adapt_tol=true, cutoff=nothing, sc=1.0, tol_targetAcceptance=0.10,
             rng=MersenneTwister(Random.make.seed())], dist=ABCdefaultDist,
             record_summaries=true)

Sample from ABC posterior using tolerance adaptive ABC-MCMC.

# Arguments
- `theta_0`: Initial value of parameter vector (Vector{Float64})
- `prior`: Function returning prior log density values.
- `s_obs`: Observed summary statistics (Vector{Float64})
- `sim!`: Function simulating summaries; sim!(s, θ, rng) simulates observed
          summaries corresponding to parameters θ and records them to s.
- `n`: Length of MCMC chain.
- `tol`: Tolerance (initial tolerance if `adapt_tol=true`).
- `b`: Burn-in length. The total number of simulations is b+n.
- `adapt_cov`: Whether covariance adaptation is done.
- `adapt_tol`: Whether tolerance adaptation is done.
- `cutoff`: Cut-off function ϕ. If `nothing`, simple cut-off is used,
            "gaussian" => Gaussian, "epa" => Epanechnikov. For other cut-offs, specifies
            the desired function.
- `sc`: Scaling of proposal distribution (only used if `adapt_cov=false`)
- `tol_targetAcceptance`: Target acceptance rate of tolerance adaptation.
- `rng`: Random number generator
- `dist`: Distance function (default Euclidean distance).
- `record_summaries`: Whether the summaries should be recorded.
- `out`: Output ABCOut struct with fields `Theta` (matrix of d×n, each column
         is simulated parameter), `Dist` (vector of corresponding tolerances),
         `Summary` (matrix of corresponding summaries),
         `cutoff` (the cut-off function w/ tolerance), `Stats` (various
         statistics)
"""
function abc_mcmc(theta0::Vector{Float64}, prior::Function, s_obs::Vector{Float64},
    sim!::Function, n::Int64; tol::Float64=0.0, b::Int64=convert(Int64, ceil(0.1n)),
    adapt_cov::Bool=true, adapt_tol::Bool=true,
    cutoff::Union{Nothing,String,Function,ABCCutoff}=nothing, sc=1.0, stepSize_eta=nothing,
    tol_targetAcceptance::Float64=0.10, rng=MersenneTwister(Random.make_seed()),
    record_summaries::Bool=true, dist=ABCdefaultDist)

    d = length(theta0)
    theta = deepcopy(theta0); theta_ = zeros(d)

    d_obs = length(s_obs)
    s = zeros(d_obs); s_ = zeros(d_obs)

    p_theta = prior(theta)
    y_dist = Inf64

    # Ensure that initial distance is finite & postive:
    while !isfinite(y_dist) || y_dist < 0.0
        sim!(s, theta, rng)
        y_dist = dist(s, s_obs)
    end

    if tol == 0.0
        tol = y_dist + (y_dist == 0.0)
    end
    if adapt_tol
        tolerance = AdaptiveMCMC.AdaptiveScalingMetropolis(1, tol_targetAcceptance)
        tolerance.sc = 1.0/tol
        tols_ = zeros(b)
    else
        tols_ = nothing
    end
    if typeof(cutoff) == ABCCutoff
        phi = cutoff
    elseif typeof(cutoff) == String
        if cutoff=="gaussian"
            phi = ABCGaussCutoff(tol)
        elseif cutoff=="epa"
            phi = ABCEpaCutoff(tol)
        else
            error("Unknown cutoff")
        end
    else
        phi = ABCCutoff(tol, cutoff)
    end

    if adapt_cov
        proposal = AdaptiveMCMC.AdaptiveMetropolis(d)
        proposal.m = theta0
        if adapt_tol
            # Use 'compatible' adaptation...
            proposal.step.eta = tolerance.step.eta
        end
    else
        proposal = AdaptiveMCMC.ConstantShape(d, sc)
    end

    if stepSize_eta != nothing
        proposal.step.eta = stepSize_eta
        if adapt_tol
            tolerance.step.eta = stepSize_eta
        end
    end
    proposal_burnin = proposal

    if record_summaries
        Summary = Array{Float64}(undef,d_obs,n)
    else
        Summary = missing
    end

    y_phi = phi(y_dist)
    if y_phi == 0.0
        # If fail to accept the initial observation, enforce acceptance
        y_phi = max(phi.fun(1.0), eps(Float64)) # phi.fun(1.0)
        y_dist = phi.tol
    end
    Theta = Array{Float64}(undef,d,n)
    Dist = Vector{Float64}(undef,n)
    all_acc = 0; acc = 0
    z = zeros(d)
    for k in 1:(n+b)
        #theta_ = theta + 0.5*randn(d)
        AdaptiveMCMC.draw!(proposal, rng)
        theta_ .= theta
        theta_ .+= proposal.y
#        theta_ = theta + z
        p_theta_ = prior(theta_)
        sim!(s_, theta_, rng)
        y_dist_ = dist(s_, s_obs)
#        y_phi_ = phi.fun(y_dist_/phi.tol)
        y_phi_ = phi(y_dist_)
        α = min(1.0, exp(p_theta_ - p_theta)*y_phi_/y_phi)
        accept = (rand(rng) < α)
        if accept
            theta .= theta_; p_theta = p_theta_;
            y_dist = y_dist_; y_phi = y_phi_;
            s, s_ = s_, s
            all_acc += 1
            acc += (k>b)
        end
        AdaptiveMCMC.adapt!(proposal, theta, α)
        if adapt_tol && k <= b
            AdaptiveMCMC.adapt!(tolerance, theta, α)
            phi.tol = tols_[k] = 1.0/tolerance.sc
        end
        if (k == b)
            proposal_burnin = deepcopy(proposal)
        end
        if (k > b)
            Theta[:,k-b] .= theta
            Dist[k-b] = y_dist
            if record_summaries
                Summary[:,k-b] .= s
            end
        end
    end
    ABCOut(Theta, Dist, Summary, s_obs, phi, ABCStats(all_acc/(n+b), acc/n, proposal, proposal_burnin, tols_))
end

"""
    iact(x)

Calculate integrated autocorrelation of the sequence 'x' using adaptive window
truncated autocorrelation sum estimator.
"""
function iact(X::Vector{Float64})
    n = length(X)
    # Calculate centred & normalised X
    X_ = (X.-mean(X))/sqrt(var(X))
    # ...and this gives then ACF at specified lag
    C = max(5.0, log10(n)) # Suggested by Sokal according to
                           # http://dfm.io/posts/autocorr/
    tau = 1.0
    for k in 1:(n-1)
        tau += 2.0*dot(X_, 1:(n-k), X_, (1+k):n)/(n-k)
        if k>C*tau
            break
        end
    end
    max(0.0, tau)
end

# When correction kernel is simple, we may calculate estimates for all epsilon
# (and then find the suitable epsilon among)
function is_est(X::Matrix{Float64}, D::Vector{Float64}, phi::ABCCutoff,
    kernel::ABCSimpleCutoff, eps; normal_quantile=1.96)
    eps0 = phi.tol

    d, n = size(X)
    I = sortperm(D); D_ = D[I]
    if typeof(eps) == Nothing
        n_eps = n
        eps = D_
    else
        n_eps = length(eps)
    end

    E = Matrix{Float64}(undef,d,n)
    S = Matrix{Float64}(undef,d,n)
    E_eps = Matrix{Float64}(undef,d,n_eps)
    S_eps = Matrix{Float64}(undef,d,n_eps)
    ci_L = Matrix{Float64}(undef,d,n_eps)
    ci_U = Matrix{Float64}(undef,d,n_eps)

    I = sortperm(D); D_ = D[I]
    phi_0 = phi.(D_)
    invalid = phi_0 .== 0.0
    xi_1 = 1.0./phi_0
    xi_1[invalid] .= 0.0
    xi_1 /= sum(xi_1)
    #xi_1 = ones(n)/n
    for i in 1:d
        X_ = X[i,I]
        xi_f = X_.*xi_1
        E[i,:] = cumsum(xi_f)./cumsum(xi_1)
        S[i,:] = (cumsum(xi_f.^2) - 2E[i,:].*cumsum(xi_f.*xi_1)
                  + cumsum(xi_1.^2).*E[i,:].^2) ./ cumsum(xi_1).^2
        #S[i,:] = (cumsum(X_.^2) - 2cumsum(X_).*E[i,:] +
        #                 (1:n).*(E[i,:].^2))./(1:n).^2
    end
    if typeof(eps) == Nothing
        E_eps = E; S_eps = S
    else #if typeof(eps0) != Void
        #all(eps .<= D_[end]) || error("All ϵ's should be smaller than the maximum distance")
        j = 1
        for k in 1:n_eps
            while j<n && D_[j+1] < eps[k]
                j += 1
            end
            E_eps[:,k] = E[:,j]
            S_eps[:,k] = S[:,j]
        end
    end
    for i in 1:d
        dx = normal_quantile*sqrt.(max.(S_eps[i,:],0)*iact(X[i,:]))
        ci_L[i,:] = E_eps[i,:] - dx; ci_U[i,:] = E_eps[i,:] + dx
    end
    ABCEst(E_eps, S_eps, eps, ci_L, ci_U, normal_quantile)
end

# For general correction, calculate directly from the definition
function is_est(X::Matrix{Float64}, D::Vector{Float64}, phi::ABCCutoff,
    kernel::ABCGenCutoff, eps; normal_quantile=1.96)
    typeof(eps) != Nothing || error("Must supply ϵ with general cutoff")
    d, n = size(X)
    n_eps = length(eps)
    E = Matrix{Float64}(undef,d,n_eps)
    S = Matrix{Float64}(undef,d,n_eps)
    ci_L = Matrix{Float64}(undef,d,n_eps)
    ci_U = Matrix{Float64}(undef,d,n_eps)
    # Evaluate the cutoff for eps0, and the positive indices
    phi_0 = phi.(D)
    invalid = phi_0 .== 0.0
    for k in 1:n_eps
        U = kernel.fun.(D/eps[k]) ./ phi_0
        U[invalid] .= 0.0
        W = U/sum(U)
        for i in 1:d
            E[i,k] = sum(W .* X[i,:])
            S[i,k] = sum(W.^2 .* (X[i,:] .- E[i,k]).^2)
        end
    end
    for i in 1:d
        dx = normal_quantile*sqrt.(max.(S[i,:],0)*iact(X[i,:]))
        ci_L[i,:] = E[i,:] - dx
        ci_U[i,:] = E[i,:] + dx
    end
    ABCEst(E, S, eps, ci_L, ci_U, normal_quantile)
end

"""
    est = abc_postprocess(out, f, [tol]; regress=false, kernel=out.cutoff,
                          normal_quantile=1.96)

Calculate post-correction estimates for ABC-MCMC output.

# Arguments
- `out`: Output of the `abc_mcmc` function.
- `f`: Function of interest.
- `tol`: Vector of tolerance values for which estimates are calculated.
         If not given (or `nothing`), and `out.cutoff` is simple, then
         correction calculated to all tolerances.
- `regress`: Whether regression correction should be used.
- `kernel`: Kernel for post-correction. (NB kernel.tol is ignored!)
- `normal_quantile`: The desired normal quantile (default 1.96 -> 95% coverage;
                     any other quantile may be calculated using Distributions as:
                     `-quantile(Normal(), (1.0-coverage)/2)`
- `est`: Estimates
"""
function abc_postprocess(out::ABCOut, f::Function,
    tol::Union{Nothing,Vector{Float64}}=nothing;
    regress=false, kernel=out.cutoff, normal_quantile=1.96)
    if regress
        if typeof(out.Summary) == Missing
            error("Summary statistics not recorded; please re-run abc_mcmc with 'record_summaries=true'")
        end
        if typeof(tol) == Nothing
            error("Must supply tolerances with regression correction")
        end
        return abc_regress(out, f, tol; kernel=kernel.fun,
                                        normal_quantile=normal_quantile)
    else
        F = mapslices(f, out.Theta, dims=1)
        return is_est(F, out.Dist, out.cutoff, kernel, tol;
                      normal_quantile=normal_quantile)
    end
end

@inline function safe_regress!(coef, XX, X, W, F_)
    try
        coef .= XX\(X'F_)
    catch
        @warn "Regression failed"
        coef .= 0.0
        coef[1] = dot(W, F_)
    end
    nothing
end

@inline function intercept_var_factor(XWX::Matrix{Float64})
    try
        max(0.0, inv(XWX)[1,1])
    catch
        Inf64
    end
end


"""
    est = abc_regress(out, f, tol; kernel=out.cutoff.fun)

Calculate post-correction estimates for ABC-MCMC output using regression
correction of Beaumont (2002), and producing approximate confidence
intervals similar to `abc_postprocess`

# Arguments
- `out`: Output of the `abc_mcmc` function (with `record_summaries=true`)
- `f`: Function of interest.
- `tol`: Vector of tolerance values for which estimates are calculated.
- `kernel`: Kernel function used in regression correction.
- `est`: Estimates which may be inspected by `ABCPlots.plot_abc(est)`
"""
function abc_regress(out::ABCOut, f::Function, tol::Vector{Float64};
    kernel::Function=out.cutoff.fun, normal_quantile=1.96)
    if typeof(out.Summary) == Missing
        error("Summary statistics not recorded; please re-run abc_mcmc with 'record_summaries=true'")
    end
    F = mapslices(f, out.Theta, dims=1)
    (d_f,n) = size(F)
    n_tol = length(tol)
    E = Matrix{Float64}(undef, d_f, n_tol)
    S = Matrix{Float64}(undef, d_f, n_tol)
    ci_L = Matrix{Float64}(undef, d_f, n_tol)
    ci_U = Matrix{Float64}(undef, d_f, n_tol)

    d_s = size(out.Summary, 1)
    X = Matrix{Float64}(undef, n, d_s+1)
    WX = Matrix{Float64}(undef, n, d_s+1)
    W = Vector{Float64}(undef, n)
    F_ = Vector{Float64}(undef, n)
    phi_0 = out.cutoff.(out.Dist)
    invalid = phi_0 .== 0.0

    # Construct the regression data matrix X:
    for i in 1:n
        X[i, 1] = 1.0
        X[i, 2:end] .= out.Summary[:,i] .- out.s_obs
    end

    # Calculate the integrated autocorrelation for
    # regression-corrected samples (using all!)
    tau0 = Vector{Float64}(undef, d_f)
    XX = X'*X
    W .= 1.0/n

    coef = Vector{Float64}(undef, d_s+1)

    for j in 1:d_f
        F_ .= F[j,:]
        safe_regress!(coef, XX, X, W, F_)
        F_ .= F[j,:]
        F_ -= X*coef
        tau0[j] = iact(F_)
    end

    for i in 1:n_tol
        tol_ = tol[i]
        # Calculate unnormalised weights
        map!(x -> kernel(x/tol_), W, out.Dist)
        W .= W./phi_0
        W[invalid] .= 0.0

        # Normalise (and warn if cannot)
        Ws = sum(W)
        if Ws == 0.0
            @warn string("No matching observations for tolerance ", tol_)
            continue
        end
        W /= Ws

        # Calculate weighted regression
        WX .= X
        @inbounds for j in 1:n
            WX[j,:] *= W[j]
        end
        # The 'projection matrix' for tolerance tol_
        XWX = X'*WX
        for j in 1:d_f
            F_ .= F[j,:]
            # Calculate the regression coefficients:
            safe_regress!(coef, XWX, WX, W, F_)
            #coef = XWX\(WX'F_)
            # The mean estimate is simply the intercept:
            alpha = coef[1]
            E[j,i] = alpha
            # F_ = -(regression-corrected values)
            mul!(F_, X, coef)
            F_ -= F[j,:]
            # Calculate S:
            s_ = 0.0
            @inbounds for k in 1:n
                s_ += W[k]^2 * F_[k]^2
            end
            s_ *= intercept_var_factor(XWX)
            S[j,i] = s_
            # Confidence bounds:
            dx = normal_quantile*sqrt(s_*tau0[j])
            ci_L[j,i] = alpha - dx
            ci_U[j,i] = alpha + dx
        end
    end
    ABCEst(E, S, tol, ci_L, ci_U, normal_quantile)
end

end # module
