using JLD2, FileIO, LinearAlgebra
using Statistics: mean

function test_compare_merge(fname_base="test_Laplace_results_", files=100, verbose=false)
    if files < 1
        return
    end
    if verbose
        println(string("Loading ", fname_base, 1, ".jld2"))
    end
    o = load(string(fname_base, 1, ".jld2"))
    E_ = o["E"]
    L_ = o["L"]
    U_ = o["U"]
    E_reg_ = o["E_reg"]
    L_reg_ = o["L_reg"]
    U_reg_ = o["U_reg"]
    Acc_ = o["Acc"]
    tol_ = o["tol"]
    for i in 2:files
        if verbose
            println(string("Loading ", fname_base, i, ".jld2"))
        end
        o = load(string(fname_base, i, ".jld2"))
        E_ = cat(E_,o["E"]; dims=3)
        L_ = cat(L_,o["L"]; dims=3)
        U_ = cat(U_,o["U"]; dims=3)
        E_reg_ = cat(E_reg_,o["E_reg"]; dims=3)
        L_reg_ = cat(L_reg_,o["L_reg"]; dims=3)
        U_reg_ = cat(U_reg_,o["U_reg"]; dims=3)
        Acc_ = cat(Acc_,o["Acc"]; dims=1)
    end
    E_, L_, U_, E_reg_, L_reg_, U_reg_, Acc_, tol_
end

function test_compare_postprocess(E, L, U, m=(0.0,nothing); weighted=false)
    d, n_out, n_rep, _ = size(E)
    mu = zeros(d, n_out)
    # Calculate CI coverages
    P = Vector{LowerTriangular}(undef, d)
    V = deepcopy(P)
    for i in 1:d
        P[i] = LowerTriangular(zeros(n_out,n_out))
        V[i] = deepcopy(P[i])
        for k in 1:n_out
            E_ = E[i,k,:,k:end]
            mu_ = mapslices(mean, E_, dims=1)
            v_ = mapslices(var, E_, dims=1)
            if weighted
                w_ = 1.0./v_; w_ /= sum(w_)
                mu[i,k] = dot(mu_, w_)
            else
                mu[i,k] = mean(mu_)
            end
        end
        for k in 1:n_out
            E_ = E[i,k,:,k:end]
            if m == nothing || m[i] == nothing
                m_ = mu[i,k]
            else
                if ndims(m) == 2
                    m_ = m[i,k]
                else
                    m_ = m[i]
                end
            end
            L_ = L[i,k,:,k:end]
            U_ = U[i,k,:,k:end]
            P[i][k:end,k] = mapslices(mean, L_ .< m_ .< U_, dims=1)
            V[i][k:end,k] = mapslices(mean, (E_ .- m_).^2, dims=1)
        end
    end
    P, mu, V
end

function print_rmse(tol, V, mAcc=nothing; prefix_line="")
    M_ = Vector(undef, length(V))
    e_ = Vector{Float64}(undef, length(V))
    for i in 1:length(V)
        V_ = sqrt.(V[i])
        e_[i] = floor(maximum(log10.(V_)))
        M_[i] = V_/10^e_[i]
    end
    if typeof(mAcc) == Nothing
        mAcc = zeros(length(tol),0)
    end
    println("RMSEs (×10^$(e_) for f_", collect(1:length(V)), ", respectively)")
    print_latex_table(hcat(tol, M_..., mAcc); prefix_line=prefix_line)
    nothing
end

function ground_mean(E, only_direct=false)
    d, n_tol = size(E)[1:2]
    mu = zeros(d, n_tol)
    for k in 1:n_tol
        if only_direct
            mu[:,k] = E[:,k,k]
        else
            mu[:,k] = mapslices(mean, E[:,k,k:end], dims=2)
        end
     end
    mu
end
