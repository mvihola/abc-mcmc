# This takes about 2h to run with MacBook Pro (Retina, 13-inch, Early 2015)
using JLD2

n = 10_000
if length(ARGS) < 2
    n_rep = 10_000
    ofname = joinpath(@__DIR__, "output", "test_Normal_GaussCutoff_results_all.jld2")
elseif length(ARGS) == 2
    n_rep = parse(Int64, ARGS[1])
    ofname = ARGS[2]
end

tol = collect(Base.LinRange(0.10, 3.0, 5))
model = joinpath(@__DIR__, "Normal.jl")
cutoff = "gaussian"
theta_0 = [0.0]
verbose = true
include(joinpath(@__DIR__, "test_Generic.jl"))

@save ofname E L U Acc tol
