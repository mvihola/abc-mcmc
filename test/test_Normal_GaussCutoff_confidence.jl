using JLD2
include(joinpath(@__DIR__, "Helpers.jl"))

@load "output/test_Normal_GaussCutoff_results_all.jld2"
P, mu, V = test_compare_postprocess(E, L, U, (0.0, nothing))
mAcc = mapslices(mean, Acc, dims=1)'

println("Frequencies for confidence intervals:")
print_latex_table(hcat(ϵ, P..., mAcc))

M_ = Vector(undef, length(V))
for i in 1:length(V)
    V_ = sqrt.(V[i])
    e_[i] = -2
    M_[i] = V_/10^(-2)
end

println("RMSEs (×10^$(e_) for f_", collect(1:length(V)), ", respectively) & accept rate:")
print_latex_table(hcat(ϵ, M_..., mAcc))
