using JLD2
include(joinpath(@__DIR__, "Helpers.jl"))
include(joinpath(@__DIR__, "test_compare_postprocess.jl"))
include(joinpath(@__DIR__, "test_adapt_postprocess.jl"))

include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC, AdaptiveMCMC

@load "output/LotkaVolterra_ground.jld2"
mu = ground_mean(E)
mu_reg = ground_mean(E_epa_reg)
m = mu[:,1]
m_reg = mu_reg[:,1]

# Calculate these for reference:
@load "output/test_LotkaVolterra_results_all.jld2"
P, _, V = test_compare_postprocess(E, L, U, mu)
P_reg, _, V_reg = test_compare_postprocess(E_reg, L_reg, U_reg, mu_reg)

# Calculate these for reference:
@load "output/test_LotkaVolterra_stepSize_results_all.jld2"
P_step, _, V_step = test_compare_postprocess(E, L, U, mu)
P_step_reg, _, V_step_reg = test_compare_postprocess(E_reg, L_reg, U_reg, mu_reg)

target_tols = [80.0]
f(x) = exp.(x)
@load "output/test_LotkaVolterra_Adaptation_results_all.jld2"
Final_tol = map(o->o.cutoff.tol, out)
valid = Final_tol .> target_tols[1]
E_, S_ = adapt_postcorrect(out[valid], target_tols, f)
E_reg_, S_reg_ = adapt_postcorrect(out[valid], target_tols, f;
                                   regress=true, kernel=ABCEpaCutoff(1.0))

P_adapt =  verify_confidence(E_, S_, [0.95]; m=m)
V_adapt = normalised_std(E_, m)
P_adapt_reg =  verify_confidence(E_reg_, S_reg_, [0.95]; m=m_reg)
V_adapt_reg = normalised_std(E_reg_, m_reg)

function form_rmse_table(V_, V_adapt_, modifier=[100,10_000,100])
    tbl = sqrt.([hcat(map(v->v[:,1], V_)...); V_adapt_'])
    for i in 1:length(modifier)
        tbl[:,i] *= modifier[i]
    end
    tbl
end
tbl = form_rmse_table(V, V_adapt)
tbl_step = form_rmse_table(V_step, V_adapt)
tbl_reg = form_rmse_table(V_reg, V_adapt_reg)
tbl_step_reg = form_rmse_table(V_step_reg, V_adapt_reg)

println("RMSE comparison:")
print_latex_table([hcat(tol..., mean(Final_tol)); tbl'])
println("RMSE comparison (regression):")
print_latex_table([hcat(tol..., mean(Final_tol)); tbl_reg'])
println("RMSE comparison (step):")
print_latex_table([hcat(tol..., mean(Final_tol)); tbl_step'])
println("RMSE comparison (step, regression):")
print_latex_table([hcat(tol..., mean(Final_tol)); tbl_step_reg'])

println("RMSE comparison (both):")
print_latex_table(vcat(hcat(tol..., mean(Final_tol),
                            tol..., mean(Final_tol)), [tbl' tbl_reg']))
println("RMSE comparison (step, both):")
print_latex_table(vcat(hcat(tol..., mean(Final_tol),
                           tol..., mean(Final_tol)), [tbl_step' tbl_step_reg']))


function form_ci_table(P_, P_adapt_)
    [hcat(map(v->v[:,1], P_)...); P_adapt_[:,:,1]']
end
tbl_P = form_ci_table(P, P_adapt)
tbl_P_reg = form_ci_table(P_reg, P_adapt_reg)
tbl_P_step = form_ci_table(P_step, P_adapt)
tbl_P_step_reg = form_ci_table(P_step_reg, P_adapt_reg)

println("CI comparison:")
print_latex_table([hcat(tol..., mean(Final_tol)); tbl_P'])
println("CI comparison (regression):")
print_latex_table([hcat(tol..., mean(Final_tol)); tbl_P_reg'])
println("CI comparison (step):")
print_latex_table([hcat(tol..., mean(Final_tol)); tbl_P_step'])
println("CI comparison (step, regression):")
print_latex_table([hcat(tol..., mean(Final_tol)); tbl_P_step_reg'])

println("CI comparison (both):")
print_latex_table(vcat(hcat(tol..., mean(Final_tol),
                            tol..., mean(Final_tol)), [tbl_P' tbl_P_reg']))
