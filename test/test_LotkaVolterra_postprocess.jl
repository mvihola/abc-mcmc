include(joinpath(@__DIR__, "test_compare_postprocess.jl"))
include(joinpath(@__DIR__, "Helpers.jl"))

fname_base = "output/test_LotkaVolterra_results_"
E, L, U, E_reg, L_reg, U_reg, Acc, tol = test_compare_merge(fname_base, 100)
ofname = string(fname_base, "all.jld2")
@save ofname E L U E_reg L_reg U_reg Acc tol
