using Random
Random.seed!(12345)

using JLD2
using LinearAlgebra
using Plots
using FileIO

include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC
using ABCPlots
include(joinpath(@__DIR__, "Helpers.jl"))

function yaxis_labels!(p)
    plot!(p[1], ylabel="\\theta")
    plot!(p[2], ylabel="|\\theta|")
    nothing
end
function xaxis_labels!(p)
    plot!(p[end], xlabel="\\epsilon")
    nothing
end


o = load("output/test_Normal_GaussCutoff_results_all.jld2")
p = Plots_all_estimates(o["tol"], o["E"])
xaxis_labels!(p)
d_f = length(p)
n = size(o["E"])[3]

include(joinpath(@__DIR__, "Normal.jl"))
tol = o["tol"][end]
out = abc_mcmc(theta_0, prior, s_obs, sim!, n; cutoff="gaussian",
               tol=tol, adapt_cov=true, adapt_tol=false)
tols = collect(LinRange(o["tol"][1], tol, 256))
est = abc_postprocess(out, f, tols)

p_single = plot_abc(est, 0.0)
yaxis_labels!(p_single)
xaxis_labels!(p_single)
for i in 1:d_f
    ylim_ = Plots.get_sp_lims(p[i].subplots[1], :y)
    Plots.plot!(p_single[i], ylim = ylim_, xlim = (minimum(o["tol"]),maximum(o["tol"])),
                xticks=o["tol"])
end

Plots.plot(permutedims(cat(p_single, p, dims=2))...,
           layout=(d_f,2), size=(800,400*2/3))
Plots.savefig("Normal_GaussCutoff_both.pdf")
