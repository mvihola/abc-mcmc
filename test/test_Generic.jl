# Include this after defining
# model -- string of model file name, which defines
#          "prior", "s_obs", "sim!" and "f"
# n     -- number of MCMC iterations
# tol   -- vector (or iterator) of tolerance values
# n_rep -- number of repetitions
# f     -- function to use
# abc_options
# abc_postprocess_options
# abc_postprocess_regress_options

using SharedArrays

include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC

include("$model")

(@isdefined verbose) || (verbose = false)
(@isdefined cutoff) || (cutoff = nothing)

if !(@isdefined abc_options)
	abc_options = Dict(:cutoff => cutoff,
	                   :adapt_cov => true,
					   :adapt_tol => false)
end
if !(@isdefined abc_postprocess_options)
	abc_postprocess_options = Dict()
end
if !(@isdefined abc_postprocess_regress_options)
	abc_postprocess_regress_options = abc_postprocess_options
end


n_out = length(tol)

if typeof(theta_0) == Array{Float64,3}
  different_theta_0=true
  d = length(f(theta_0[1,1,:]))
  d_theta = length(theta_0[1,1,:])
else
  different_theta_0=false
  d = length(f(theta_0))
  d_theta = length(theta_0)
end

E = Array{Float64}(undef,d,n_out,n_rep,n_out)
L = Array{Float64}(undef,d,n_out,n_rep,n_out)
U = Array{Float64}(undef,d,n_out,n_rep,n_out)
E_reg = Array{Float64}(undef,d,n_out,n_rep,n_out)
L_reg = Array{Float64}(undef,d,n_out,n_rep,n_out)
U_reg = Array{Float64}(undef,d,n_out,n_rep,n_out)
Acc = Array{Float64}(undef,n_rep,n_out)
Cov = Array{Float64}(undef,d_theta,d_theta,n_rep,n_out)
Cov_b = Array{Float64}(undef,d_theta,d_theta,n_rep,n_out)

#Threads.@threads
for k in 1:n_rep
	  #println(k)
	  verbose && println(k)
      for j in 1:n_out
        if different_theta_0
          theta_0_ = theta_0[k,j,:]
        else
          theta_0_ = theta_0
        end
        out = abc_mcmc(theta_0_, prior, s_obs, sim!, n;
		               tol=tol[j], record_summaries=true, abc_options...)
        est = abc_postprocess(out, f, convert(Vector, tol[1:j]);
		                      abc_postprocess_options...)
		est_reg = abc_postprocess(out, f, convert(Vector, tol[1:j]); regress=true,
		                      abc_postprocess_regress_options...)
        E[:,1:j,k,j] = est.E
        L[:,1:j,k,j] = est.ci_L
        U[:,1:j,k,j] = est.ci_U
		E_reg[:,1:j,k,j] = est_reg.E
        L_reg[:,1:j,k,j] = est_reg.ci_L
        U_reg[:,1:j,k,j] = est_reg.ci_U
        Acc[k,j] = out.Stats.acc
		Cov[:,:,k,j] = Matrix(out.Stats.adapt.L)
		Cov_b[:,:,k,j] = Matrix(out.Stats.adapt_burnin.L)
    end
end

nothing
