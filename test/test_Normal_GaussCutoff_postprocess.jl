include(joinpath(@__DIR__, "test_compare_postprocess.jl"))
include(joinpath(@__DIR__, "Helpers.jl"))

fname_base = "output/test_Normal_GaussCutoff_results_"
E, L, U, Acc, ϵ = test_compare_merge(fname_base, 100)
ofname = string(fname_base, "all.jld2")
@save ofname E L U Acc ϵ
