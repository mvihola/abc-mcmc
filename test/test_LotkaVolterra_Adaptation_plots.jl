using Plots
using Random
using JLD2
include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC, AdaptiveMCMC

include(joinpath(@__DIR__, "test_adapt_postprocess.jl"))

# All results, perhaps after running test_adapt_merge(), are assumed
# to be found in this file:
@load "output/test_LotkaVolterra_Adaptation_results_all.jld2"

Log_tols = cat(map(o->log.(o.Stats.tol), out)..., dims=2)
Acc = map(o->o.Stats.acc, out)

# Shows development of tolerances during burn-in:
pq = plot_tolerance_quantiles(Log_tols; plotSize=(500,170),
                         fname="LotkaVolterra_tolerance_adaptation.pdf")

# Enforce upper limit 7.0:
ylim_ = Plots.get_sp_lims(pq.subplots[1], :y)
Plots.ylims!(pq, ylim_[1], 6.2)

# Shows histogram of acceptance rates after burn-in:
ph = Plots.histogram(Acc, fillcolor="lightgray", legend=false,
                    size=(300,170), xlabel="acceptance rate")
Plots.savefig(ph, "LotkaVolterra_adaptation_accrate.pdf")

Plots.plot(pq, ph, layout=(1,2), size=(800,170))
Plots.savefig("LotkaVolterra_Adaptation_both.pdf")
