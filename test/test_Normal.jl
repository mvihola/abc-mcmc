using JLD2

n = 10_000
if length(ARGS) < 2
    n_rep = 10_000
    ofname = joinpath(@__DIR__, "output", "test_Normal_results_all.jld2")
elseif length(ARGS) == 2
    n_rep = parse(Int64, ARGS[1])
    ofname = ARGS[2]
end

tol = Base.LinRange(0.10, 3.0, 5)
model = joinpath(@__DIR__, "Normal.jl")
verbose = true
theta_0 = [0.0]

include(joinpath(@__DIR__, "test_Generic.jl"))

using Statistics: mean
ϵ = collect(tol)

#@save ofname E L U Acc ϵ
@save ofname E L U E_reg L_reg U_reg Acc ϵ theta_0
