# About 2h40min with Intel i5-4460
n = 10_000
b = 10_000

n_rep = 1000
ofname = joinpath(@__DIR__, "output", "test_LotkaVolterra_Adaptation_results_all.jld2")

using Random
using JLD2
include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC

d = 3
include(joinpath(@__DIR__, "LotkaVolterra.jl"))
theta_0 = zeros(n_rep, d)
out = Vector{ABCOut}(undef, n_rep)
for i=1:n_rep
    println(i,"/", n_rep)
    theta_0[i,:] = draw_from_prior()
    out[i] = abc_mcmc(theta_0[i,:], prior, s_obs, sim!, n; b=b, record_summaries=true)
end

@save ofname theta_0 out
