using JLD2
using FileIO
using LinearAlgebra
using Statistics
using Distributions
using Plots

include(joinpath(@__DIR__, "..", "Setup.jl"))

using ABC

function test_adapt_merge(fname_base="test_Laplace_results_", files=100)
    if files < 1
        return
    end
    o = load(string(fname_base, 1, ".jld2"))
    Acc = o["acc"]
    Final_tol = o["final_tol"]
    Log_tols = cat(o["log_tols"]..., dims=2)
    Thetas = cat(o["thetas"]..., dims=3)
    Dists = cat(o["dists"]..., dims=2)
    for i in 2:files
        o = load(string(fname_base, i, ".jld2"))
        Acc = cat(Acc,o["acc"], dims=1)
        Final_tol = cat(Final_tol,o["final_tol"], dims=1)
        Log_tols = cat(Log_tols, o["log_tols"]..., dims=2)
        Thetas = cat(Thetas, o["thetas"]..., dims=3)
        Dists = cat(Dists, o["dists"]..., dims=2)
    end
    Acc, Final_tol, Log_tols, Thetas, Dists
end

function adapt_postcorrect(out::Vector{ABCOut},
                           target_tols::Vector{Float64}=[100.0],
                           fun = x -> exp.(x); postprocess_options...)
    n_rep = length(out)
    n = length(out[1].Dist)
    d = length(fun(out[1].Theta[:,1,1]))
    n_tol = length(target_tols)
    E = zeros(d,n_tol,n_rep)
    S = deepcopy(E)
    for i in 1:n_rep
        est = abc_postprocess(out[i], fun, target_tols; postprocess_options...)
        E[:,:,i] = est.E
        S[:,:,i] = (est.ci_U - est.E)/est.normal_quantile
    end
    E, S
end

function plot_tolerance_quantiles(Tols; q=[0.5,0.75,0.95,0.99], fname=nothing,
    plotSize=(500,300))
    to_file = typeof(fname) == String
    d, n_rep = size(Tols)
    q_ = (1.0.-q)/2
    qq = vcat(q_,0.5,1.0.-q_[end:-1:1])
    n_q = length(q)
    alpha = 0.8/n_q
    Q = mapslices(x -> quantile(x, qq), Tols, dims=(2))
    Qmed = Q[:,n_q+1]
    p = Vector{Plots.Plot}(undef, n_q)

    p = Plots.plot(Qmed, ribbon=(Qmed-Q[:,n_q],Q[:,end-n_q+1]-Qmed),
                   seriescolor="black", fillalpha=alpha, legend=false,
                   size=plotSize, formatter=:plain)
    for k in 1:n_q-1
        Plots.plot!(p, Qmed, ribbon=(Qmed-Q[:,k],Q[:,end-k+1]-Qmed),
        linecolor=false, fillcolor="black", fillalpha=alpha, legend=false,
        xlabel="k (burn-in iteration)", ylabel="log \\delta_k")
    end
    if to_file
        Plots.savefig(p, fname)
    end
    p
    #Plots.plot(p)
end

function verify_confidence(E, S, q=[0.95]; m=nothing)
    d, n_tol, n_rep = size(E)
    if m == nothing
        m = mapslices(mean, E, dims=3)
    end
    n_q = length(q)
    P = zeros(d, n_tol, n_q)
    std_norm = Normal()
    for k in 1:n_q
        b = quantile(std_norm, 1 - (1 - q[k])/2)
        for i in 1:n_rep
            P[:,:,k] += abs.(E[:,:,i] - m) .<= b*S[:,:,i]
        end
    end
    P/n_rep
end

function normalised_std(E, m=nothing)
    d, n_tols, n_rep = size(E)
    if m == nothing
        m = mapslices(mean, E, dims=3)[:,:,1]
    end
    v = zeros(d, n_tols)
    for i in 1:d
        for j in 1:n_tols
            v[i,j] = mean((E[i,j,:] .- m[i,j]).^2)
        end
    end
    v
end
nothing
