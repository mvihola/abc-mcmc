#!/bin/bash -l
#SBATCH -J test_LotkaVolterra_stepSize
#SBATCH -o diag/%x_output_%A_%a.txt
#SBATCH -e diag/%x_errors_%A_%a.txt
#SBATCH -p serial
#SBATCH --array 1-100
#SBATCH -t 02:00:00
#SBATCH -n 1
#SBATCH --mem-per-cpu=4000

module load julia-env
# Out of 100 runs, max about 46min:
srun julia "${SLURM_JOB_NAME}.jl" 10 \
     "output/${SLURM_JOB_NAME}_results_${SLURM_ARRAY_TASK_ID}.jld2"
