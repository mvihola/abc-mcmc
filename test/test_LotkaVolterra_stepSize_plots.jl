using JLD2, FileIO, LinearAlgebra

include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC, AdaptiveMCMC
using ABCPlots, Plots
include(joinpath(@__DIR__, "Helpers.jl"))

function yaxis_labels!(p)
    plot!(p[1], ylabel="\\theta_1")
    plot!(p[2], ylabel="\\theta_2")
    plot!(p[3], ylabel="\\theta_3")
    nothing
end
function xaxis_labels!(p)
    plot!(p[end], xlabel="\\epsilon")
    nothing
end

o = load(joinpath(@__DIR__, "output", "test_LotkaVolterra_stepSize_results_all.jld2"))
p = Plots_all_estimates(o["tol"], o["E"])
p_reg = Plots_all_estimates(o["tol"], o["E_reg"])
xaxis_labels!(p)
xaxis_labels!(p_reg)

mcmc_tol = o["tol"][end]
min_tol = o["tol"][1]
tols = collect(LinRange(min_tol, mcmc_tol, 256))

include(joinpath(@__DIR__, "LotkaVolterra.jl"))
n = 10_000
b = 10_000
abc_options = Dict(:adapt_cov=>true, :tol=>mcmc_tol,
                   :adapt_tol=>false, :b=>b, :record_summaries=>true)
out = abc_mcmc(theta_0, prior, s_obs, sim!, n; abc_options...)
est = abc_postprocess(out, f, tols)
est_reg = abc_postprocess(out, f, tols; regress=true)

p_single = plot_abc(est, min_tol)
yaxis_labels!(p_single)
xaxis_labels!(p_single)
p_single_reg = plot_abc(est_reg, min_tol)
yaxis_labels!(p_single_reg)
xaxis_labels!(p_single_reg)
for i in 1:3
    ylim_ = Plots.get_sp_lims(p[i].subplots[1], :y)
    Plots.plot!(p_single[i], ylim = ylim_,
                xlim = (min_tol, mcmc_tol),
                xticks=o["tol"])
    ylim_ = Plots.get_sp_lims(p_reg[i].subplots[1], :y)
    Plots.plot!(p_single_reg[i], ylim = ylim_,
                xlim = (min_tol, mcmc_tol),
                xticks=o["tol"])
end

Plots.plot(permutedims(cat(p_single, p, dims=2))...,
           layout=(3,2), size=(800,373))
Plots.savefig("LotkaVolterra_stepSize_both.pdf")
Plots.plot(permutedims(cat(p_single_reg, p_reg, dims=2))...,
           layout=(3,2), size=(800,373))
Plots.savefig("LotkaVolterra_stepSize_reg_both.pdf")
