using Random
# This is a model from Boys, Wilkinson, Kirkwood: Bayesian inference for a
# discretely observed stochastic kinetic model, which was also analysed in
# Fearnhead, Prangle: Constructing summary statistics for approximate
# Bayesian computation: semi-automatic approximate Bayesian computation

using LinearAlgebra: norm, dot
using Statistics: mean, std, quantile

@inline function simulate_discrete(p::Vector{Float64}, rng=Random.GLOBAL_RNG) :: Int64
    n = length(p)
    s = 0.0; k = 1;
    U = rand(rng)
    @inbounds for k in 1:n
        s += p[k]
        if (s >= U || k==n)
            return k
        end
    end
end

@inline function apply_reaction!(x::Vector{Int64}, i::Int64) :: Nothing
    @inbounds if i == 1
        x[1] += 1             # X -> 2X
    elseif i == 2
        x[1] -= 1; x[2] += 1  # X + Y -> 2Y
    else # i == 3
        x[2] -= 1             # Y -> ∅
    end
    nothing
end

function simulate_lotka_volterra!(dT, X, x0, θ, T_end_=30.0, rng=Base.Random.GLOBAL_RNG)
    n_max = length(dT)
    x = deepcopy(x0)
    t = 0.0; n = 0; i = 0
    r = [0.0,0.0,0.0]
    sr = 0.0
    while t < T_end_ && n < n_max #&& any(x .> 0)
        r[1] = θ[1]*x[1]; r[2] = θ[2]*x[1]*x[2]; r[3] = θ[3]*x[2]
        sr = sum(r)
        dt = randexp(rng)/sr
        n += 1
        X[:,n] = x
        dT[n] = dt
        t += dt
        r ./= sr
        i = simulate_discrete(r, rng)
        apply_reaction!(x, i)
        (x[1] == 0 && x[2] == 0) && break
    end
    n
end

# Quantile of weighted sample
# Note: p must be in increasing order!
function weighted_quantile(W, X, p=[0.05,0.25,0.5,0.75,0.95])
    np = length(p)
    q = Vector{Float64}(undef, np)
    k = 1; x = -Inf64
    n = length(X)
    I = sortperm(X)
    j = 0; s = 0.0
    while true
        if s > p[k]
            q[k] = x; k += 1
            if k > np
                break
            end
        else
            j += 1
            s += W[I[j]]
            x = X[I[j]]
            if j >= n
                q[k:end] .= Inf64
                break
            end
        end
    end
    return q
end

function skeleton!(X_, dT, X, dt)
    T_end = sum(dT)
    n = length(X)
    # max possible length:
    n_ = min(Int(floor(T_end/dt))+1, length(X_))
    i = 0; t_ = 0.0; X_[1] = X[1]; k = 1
    while k < n_
        k += 1
        # Find index of next discretisation point:
        while t_ < dt && i < n
            i += 1
            t_ += dT[i]
        end
        X_[k] = X[i]
        t_ -= dt
    end
    k
end

# Autocorrelation estimator for a piecewise constant
# Markov process
function markov_acf!(X_, dT, X; dt=1.0, lags=[1])
    n_ = skeleton!(X_, dT, X, dt)
    calculate_acf!(view(X_, 1:n_), lags)
end

# Autocorrelation estimator for a piecewise constant
# Markov process
function calculate_acf!(X_, n_, lags=[1])
    X_active = view(X_, 1:n_)
    X_active .-= mean(X_active)
    X_active ./= std(X_active)
    gamma = lags * 0.0
    for k in 1:length(lags)
        lag = lags[k]
        if lag < n_
            gamma[k] = dot(X_, 1:(n_-lag), X_, (1+lag):n_)/(n_-lag)
        end
    end
    gamma
end


function lotka_volterra_stats_simple!(s, tmp, dT, X, n) :: Nothing
    X1 = view(X, 1, 1:n); X2 = view(X, 2, 1:n); dT_ = view(dT, 1:n)
    n_ = skeleton!(tmp, dT_, X1, 5.0)
    X_active = view(tmp, 1:n_)
    q1 = quantile(X_active, [0.1,0.9])
    rho = calculate_acf!(tmp, n_, [2])[1]
    n_ = skeleton!(tmp, dT_, X2, 5.0)
    X_active = view(tmp, 1:n_)
    q2 = quantile(X_active, [0.1,0.9])
    s .= [100*rho; q1; q2]
    nothing
end

macro gen_sim(n_max=50_000)
    quote
        # Preallocate these only once:
        dT = Vector{Float64}(undef, $n_max)
        X_ = Vector{Float64}(undef, $n_max)
        X = Matrix{Float64}(undef, 2, $n_max)
        # Some constants:
        x_0 = [71, 79]
        T_end = 40.0
        function sim!(s, θ, rng::AbstractRNG)::Nothing
            n = simulate_lotka_volterra!(dT, X, x_0, exp.(θ), T_end, rng)
            lotka_volterra_stats_simple!(s, X_, dT, X, n)
            nothing
        end
    end
end

# Prior for the parameters
function prior_lotka_volterra(θ)
    if all((-6.0 .< θ) .& (θ .< 0.0))
        return 0.0
    else
        return -Inf64
    end
end

# Draw sample from the prior distribution
function draw_from_prior_lotka_volterra()
    rand(3)*(-6.0)
end

# Set these for inference
draw_from_prior() = draw_from_prior_lotka_volterra()
prior(x::Vector{Float64}) = prior_lotka_volterra(x)
sim_lotka_volterra! = @gen_sim
sim!(s::Vector{Float64}, θ::Vector{Float64}, rng::AbstractRNG) = sim_lotka_volterra!(s, θ, rng)
f(x) = exp.(x)

# Observed summaries
s_obs = [-51.0711, 29.0, 304.0, 65.0, 404.0]

# For the reference: the values with which the data is simulated
theta_0 = log.([0.5,0.0025,0.3])

nothing
