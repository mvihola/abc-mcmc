# This took about 7h to run on a Macbook Pro (late 2015)
using JLD2
include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC
include(joinpath(@__DIR__, "LotkaVolterra.jl"))

n_tol = 5; d_f = 3
tols = collect(LinRange(80.0, 200.0, n_tol))

n = 10_000_000
b = 10_000
E = zeros(d_f, n_tol, n_tol)
E_reg = zeros(d_f, n_tol, n_tol)
E_epa = zeros(d_f, n_tol, n_tol)
E_epa_reg = zeros(d_f, n_tol, n_tol)
for k in 1:n_tol
    println(string(k, "/", n_tol))
    out = abc_mcmc(theta_0, prior, s_obs, sim!, n; b=b, tol=tols[k],
                   adapt_tol=false, record_summaries=true)
    E[:,1:k,k] = abc_postprocess(out, f, tols[1:k]).E
    E_reg[:,1:k,k] = abc_postprocess(out, f, tols[1:k]; regress=true).E
    E_epa[:,1:k,k] = abc_postprocess(out, f, tols[1:k]; kernel=ABCEpaCutoff(1.0)).E
    E_epa_reg[:,1:k,k] = abc_postprocess(out, f, tols[1:k]; kernel=ABCEpaCutoff(1.0), regress=true).E
end

@save "output/LotkaVolterra_ground.jld2" E E_reg E_epa E_epa_reg
