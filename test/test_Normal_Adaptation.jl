# About 10 minutes with Macbook Pro (late 2015)

n = 10_000
n_rep = 10_000
ofname = joinpath(@__DIR__, "output", "test_Normal_Adaptation_results_all.jld2")

using Random
using SharedArrays
using JLD2
include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC

d = 1
include(joinpath(@__DIR__, "Normal.jl"))
theta_0 = zeros(n_rep, d)
out = Vector{ABCOut}(undef, n_rep)
Threads.@threads for i=1:n_rep
    if Threads.threadid()==1
        println(i, "/", n_rep)
    end
    theta_0[i,:] = draw_from_prior()
    out[i] = abc_mcmc(theta_0[i,:], prior, s_obs, sim!, n)
end

@save ofname theta_0 out
