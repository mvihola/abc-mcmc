using Plots
using Random
using JLD2
include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC, AdaptiveMCMC

include(joinpath(@__DIR__, "test_adapt_postprocess.jl"))

@load "output/test_Normal_Adaptation_results_all.jld2"

Log_tols = cat(map(o->log.(o.Stats.tol), out)..., dims=2)
Acc = map(o->o.Stats.acc, out)

# Shows development of tolerances during burn-in:
pq = plot_tolerance_quantiles(Log_tols; plotSize=(500,170),
                         fname="Normal_tolerance_adaptation.pdf")

# Shows histogram of acceptance rates after burn-in:
ph = Plots.histogram(Acc, fillcolor="lightgray", legend=false,
                    size=(300,170), xlabel="acceptance rate")

Plots.plot(pq, ph, layout=(1,2), size=(800,170))
Plots.savefig("Normal_Adaptation_both.pdf")
