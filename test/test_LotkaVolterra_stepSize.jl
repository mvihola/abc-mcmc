# This takes about 15h with MacBook Pro (Retina, 13-inch, Early 2015)
using Random
Random.seed!()

n = 10_000
b = 10_000

using JLD2
if length(ARGS) < 2
    n_rep = 1_000
    ofname = joinpath(@__DIR__, "output", "test_LotkaVolterra_stepSize_results_all.jld2")
elseif length(ARGS) == 2
    n_rep = parse(Int64, ARGS[1])
    ofname = ARGS[2]
end

d = 3
#tol = Base.LinRange(100.0, 200.0, 5)
tol = collect(Base.LinRange(80.0, 200.0, 5))

model = joinpath(@__DIR__, "LotkaVolterra.jl")
#theta_0 = [-0.7498582176036779, -5.939763101465933, -1.0235511371030896]
theta_0 = [-0.55, -5.77, -1.09]
abc_options = Dict(:adapt_cov=>true,
                   :adapt_tol=>false, :b=>b, :stepSize_eta=>0.66)
include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC
abc_postprocess_regress_options = Dict(:kernel => ABCEpaCutoff(1.0))

include(joinpath(@__DIR__, "test_Generic.jl"))

@save ofname E L U E_reg L_reg U_reg Acc tol theta_0 Cov Cov_b
