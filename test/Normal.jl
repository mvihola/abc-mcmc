using Random

# Diffuse normal prior
prior(θ) = -.5(θ[1]/30.0)^2
draw_from_prior() = [randn()*30.0]

# sim! simulates "summaries" that correspond to θ
@inline function sim!(s::Vector{Float64}, θ::Vector{Float64}, rng::AbstractRNG) :: Nothing
    s[1] = θ[1] + randn(rng)
    nothing
end

s_obs = [0.0]

f(θ) = [θ; abs.(θ)]

nothing
