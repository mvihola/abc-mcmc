using JLD2
include(joinpath(@__DIR__, "Helpers.jl"))
include(joinpath(@__DIR__, "test_compare_postprocess.jl"))

@load "output/LotkaVolterra_ground.jld2"
mu = ground_mean(E)
mu_reg = ground_mean(E_epa_reg)

@load "output/test_LotkaVolterra_stepSize_results_all.jld2"
P, _, V = test_compare_postprocess(E, L, U, mu)
P_reg, _, V_reg = test_compare_postprocess(E_reg, L_reg, U_reg, mu_reg)

mAcc = mapslices(mean, Acc, dims=1)'

println("Frequencies for confidence intervals & accept rate:")
print_latex_table(hcat(tol, P..., mAcc); prefix_line="& ")

println("Frequencies for confidence intervals (regression):")
print_latex_table(hcat(tol, P_reg..., mAcc); prefix_line="& ")

println("Without regression (incl. acc. rate):")
print_rmse(tol, V,  mAcc; prefix_line="& ")
println("With regression:")
print_rmse(tol, V_reg, mAcc; prefix_line="& ")
