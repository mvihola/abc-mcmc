using Plots
using Statistics # quantile
using Printf

# Calculate quantiles of each row of a given matrix
function row_quantiles(X, p=[0.05,0.25,0.5,0.75,0.95])
    mapslices(x -> quantile(x, p), X, dims=2)
end

function Plots_boxbars!(p, x, dx, Q_; boxColor=:lightgray)
    #verrorbars(x, Q_[:,3], Q_[:,1], Q_[:,5])
    rectangle(xmin, xmax, ymin, ymax) = Plots.Shape([xmin,xmax,xmax,xmin], [ymin,ymin,ymax,ymax])
    for k in 1:length(x)
        x_ = x[k]
        Plots.plot!(p, [x_,x_], Q_[k,[1,5]], color="black")
        Plots.plot!(p, rectangle(x_-dx,x_+dx,Q_[k,2],Q_[k,4]), color=boxColor)
        Plots.plot!(p, [x_-dx,x_+dx], Q_[k,[3,3]], color="black")
        Plots.plot!(p, [x_-dx,x_+dx], Q_[k,[1,1]], color="black")
        Plots.plot!(p, [x_-dx,x_+dx], Q_[k,[5,5]], color="black")
    end
end

function Plots_all_estimates(tol, E; group_width=0.4, box_width=0.7)
    d = size(E,1)
    n_tol = length(tol)
    dtol = (tol[2]-tol[1])*group_width/n_tol
    #setfillintstyle(GR.INTSTYLE_SOLID)
    #setmarkertype(GR.MARKERTYPE_SOLID_CIRCLE)
    #setmarkersize(3.0)
    #setcolorrep(1, 0,0,0)
    box_colors = (:lightgray, :lightyellow, :lightgreen, :pink, :lightblue, :cyan)
    n_colors = length(box_colors)
    function limits_tune(xmin, xmax; gap=0.05)
        dx = xmax - xmin
        (xmin - dx*gap, xmax + dx*gap)
    end

    xmin = tol[1]-dtol; xmax = tol[end]+dtol
    p = Vector{Plots.Plot}(undef, d)
    for i in 1:d
        ymin = Inf; ymax = -Inf
        Q_ = Vector(undef,n_tol)
        for k in 1:n_tol
            Q_[k] = row_quantiles(E[i,1:k,:,k], [0.025,0.25,0.5,0.75,0.975])
            ymin = min(ymin, minimum(Q_[k]))
            ymax = max(ymax, maximum(Q_[k]))
        end
        p[i] = Plots.plot(xlim=(xmin-n_tol/2*dtol, xmax), ylim=limits_tune(ymin, ymax),
                          framestyle=:axes, gridlinewidht=3.0, gridstyle=:solid,
                          gridalpha=0.2, xticks=tol)
        for k in 1:n_tol
            #tol_ = collect(tol[1:k]) .+ (n_tol-k)*dtol
            #tol_ = collect(tol[1:k]) + (collect(k:-1:1).-(n_tol-3)/2.)*dtol
            tol_ = collect(tol[1:k]) + (collect(k:-1:1) .- 1 - (n_tol .- (1:k))/2)*dtol
            col = box_colors[rem(k-1,n_colors)+1]
            Plots_boxbars!(p[i], tol_, dtol/2*box_width, Q_[k];
                           boxColor=col)
        end
        Plots.plot!(p[i], legend=false)
    end
    p
end

function print_latex_table(A; digits=2, omit_zero=true, prefix_line="& ")
    A_ = string.(round.(A; digits=digits))
    if omit_zero
        A_[A .== 0.0] .= repeat(" ", digits+2)
    end
    print(prefix_line)
    print(join(mapslices(x->join(x," & "), A_, dims=2), "\\\\ \n$(prefix_line)"))
    println("\\\\")
end
