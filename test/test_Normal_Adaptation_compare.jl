using JLD2
include(joinpath(@__DIR__, "Helpers.jl"))
include(joinpath(@__DIR__, "test_compare_postprocess.jl"))
include(joinpath(@__DIR__, "test_adapt_postprocess.jl"))
using ABC, AdaptiveMCMC

@load "output/test_Normal_results_all.jld2"
Acc_ = Acc
P, mu, V = test_compare_postprocess(E, L, U, [0.0, nothing])
# Use the mean calculated without tolerance adaptation as 'ground truth'
m = mu[:,1]
@load "output/test_Normal_GaussCutoff_results_all.jld2"
Acc_GaussCutoff = Acc
P_GaussCutoff, mu_GaussCutoff, V_GaussCutoff = test_compare_postprocess(E, L, U, [0.0, nothing])
m_GaussCutoff = mu_GaussCutoff[:,1]


@load "output/test_Normal_Adaptation_results_all.jld2"
Final_tol = map(o->o.cutoff.tol, out)
target_tols = [0.1]
f(x) = [x; abs.(x)]
valid = Final_tol .> target_tols[1]

valid = Final_tol .>= target_tols[1]
E, S = adapt_postcorrect(out[valid], target_tols, f)

P_adapt =  verify_confidence(E, S, [0.95]; m=m)
V_adapt = normalised_std(E, m)
Final_tol_ = Final_tol

@load "output/test_Normal_GaussCutoff_Adaptation_results_all.jld2"
Final_tol = map(o->o.cutoff.tol, out)
valid = Final_tol .>= target_tols[1]
E_GaussCutoff, S_GaussCutoff = adapt_postcorrect(out[valid], target_tols, f)

P_adapt_GaussCutoff =  verify_confidence(E_GaussCutoff, S_GaussCutoff, [0.95]; m=m_GaussCutoff)
V_adapt_GaussCutoff = normalised_std(E_GaussCutoff, m_GaussCutoff)

V_both = 100*sqrt.([hcat(map(v->v[:,1], V)...); V_adapt'; hcat(map(v->v[:,1], V_GaussCutoff)...); V_adapt_GaussCutoff'])
P_both = [hcat(map(v->v[:,1], P)...);P_adapt[:,:,1]';hcat(map(v->v[:,1], P_GaussCutoff)...);P_adapt_GaussCutoff[:,:,1]']
Tol_both = [tol...,mean(Final_tol_), tol..., mean(Final_tol) ]
print_latex_table([Tol_both V_both P_both]')
