using Random
Random.seed!(12345)

using JLD2
using LinearAlgebra
using Plots
using FileIO

include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC
using ABCPlots
include(joinpath(@__DIR__, "Helpers.jl"))

function yaxis_labels!(p)
    plot!(p[1], ylabel="\\theta")
    plot!(p[2], ylabel="|\\theta|")
    nothing
end
function xaxis_labels!(p)
    plot!(p[end], xlabel="\\epsilon")
    nothing
end


@load "output/test_Normal_results_all.jld2"
p = Plots_all_estimates(tol, E)
p_reg = Plots_all_estimates(tol, E_reg)
xaxis_labels!(p)
xaxis_labels!(p_reg)

d_f = length(p)
min_tol = minimum(tol); max_tol = maximum(tol)
tolerances = collect(LinRange(min_tol, max_tol, 256))

include(joinpath(@__DIR__, "Normal.jl"))
theta_0 = [0.0]; n = 10_000
out = abc_mcmc(theta_0, prior, s_obs, sim!, n;
               tol=tol[end], adapt_cov=true, adapt_tol=false,
               record_summaries=true)
est = abc_postprocess(out, f, tolerances)
est_reg = abc_postprocess(out, f, tolerances; regress=true)

p_single = plot_abc(est, 0.0)
p_single_reg = plot_abc(est_reg, 0.0)
yaxis_labels!(p_single)
xaxis_labels!(p_single)
yaxis_labels!(p_single_reg)
xaxis_labels!(p_single_reg)

for i in 1:d_f
    ylim_ = Plots.get_sp_lims(p[i].subplots[1], :y)
    ylim_reg = Plots.get_sp_lims(p_reg[i].subplots[1], :y)
    Plots.plot!(p_single[i], ylim = ylim_,
                xlim = (min_tol, max_tol), xticks=tol)
    Plots.plot!(p_single_reg[i], ylim = ylim_reg,
                            xlim = (min_tol, max_tol), xticks=tol)
end

Plots.plot(permutedims(cat(p_single, p, dims=2))...,
           layout=(d_f,2), size=(800,400*2/3))
Plots.savefig("Normal_both.pdf")
Plots.plot(permutedims(cat(p_single_reg, p_reg, dims=2))...,
           layout=(d_f,2), size=(800,400*2/3))
Plots.savefig("Normal_reg_both.pdf")
