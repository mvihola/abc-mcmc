using Random
Random.seed!()

n = 10_000
b = 10_000
using JLD2
if length(ARGS) < 2
    n_rep = 1_000
    ofname = joinpath(@__DIR__, "output", "test_LotkaVolterra_results_all.jld2")
elseif length(ARGS) == 2
    n_rep = parse(Int64, ARGS[1])
    ofname = ARGS[2]
end

d = 3
tol = collect(Base.LinRange(80.0, 200.0, 5))
model = joinpath(@__DIR__, "LotkaVolterra.jl")
theta_0 = [-0.55, -5.77, -1.09]
abc_options = Dict(:adapt_cov=>true,
                   :adapt_tol=>false, :b=>b)
include(joinpath(@__DIR__, "..", "Setup.jl"))
using ABC
abc_postprocess_regress_options = Dict(:kernel => ABCEpaCutoff(1.0))
verbose = false

include(joinpath(@__DIR__, "test_Generic.jl"))

@save ofname E L U E_reg L_reg U_reg Acc tol theta_0 Cov
