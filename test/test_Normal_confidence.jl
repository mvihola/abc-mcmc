using JLD2
using Statistics
include(joinpath(@__DIR__, "Helpers.jl"))

@load "output/test_Normal_results_all.jld2"
P, mu, V = test_compare_postprocess(E, L, U, (0.0, nothing))
P_reg, mu_reg, V_reg = test_compare_postprocess(E_reg, L_reg, U_reg, (0.0, nothing))
mAcc = mapslices(mean, Acc, dims=1)'

println("Frequencies for confidence intervals & accept rate:")
print_latex_table(hcat(tol, P..., mAcc))
println("Frequencies for confidence intervals & accept rate (regression):")
print_latex_table(hcat(tol, P_reg..., mAcc))

M_ = Vector(undef, length(V))
e_ = Vector{Float64}(undef, length(V))
M_reg = Vector(undef, length(V_reg))
e_reg = Vector{Float64}(undef, length(V_reg))
for i in 1:length(V)
    V_ = sqrt.(V[i])
    V_reg_ = sqrt.(V_reg[i])
    e_[i] = -2
    M_[i] = V_/10^e_[i]
end

println("RMSEs (×10^(-2) for f_", collect(1:length(V)), ", respectively) & accept rate:")
print_latex_table(hcat(tol, sqrt.(V)/(10^-2)..., mAcc))
println("RMSEs (×10^(-2) for f_", collect(1:length(V_reg)), ", respectively) & accept rate:")
print_latex_table(hcat(tol, sqrt.(V_reg)/(10^-2)..., mAcc))
